# Contador de palabras

Este programa recibe como entrada un conjunto de términos en un archivo de texto y cuenta las repeticiones de cada uno. La ejecución del programa produce varios archivos nuevos. Por ejemplo:

```
$ python contador.py dormitorio.txt
```

- dormitorio_cuenta : Resultado de cálculo final.
- dormitorio_grafico : Gráfico de nube de palabras.
- dormitorio_datos : Datos de entrada emprolijados.
- dormitorio_vocabulario : Archivo para indicar sinónimos.
- dormitorio_sugerencias : Sugerencias de probables sinónimos.


## Dificultad central: reconocimiento de sinónimos

Distintos términos pueden ser equivalentes entre sí y se suman como repeticiones del mismo. La dificultad central es determinar cuáles términos son sinónimos entre sí, considerando que hay miles en cada archivo de entrada, cada uno con infinitas variantes teóricas posibles. Ya que los datos son tipeados a mano abundan variaciones plurales/singulares, formales/informales, diminutivos/aumentativos, errores ortográficos, etcétera. Además hay términos que son conceptualmente sinónimos. Por ejemplo, los siguientes términos se deben sumar como el mismo:

- mesa de luz
- Mezitas de Lux
- velador
- algunos Veladoreeees
- un belador
- la mesita chiquita de al lado de la cama

## Archivo 'entrada': datos ingresados a mano

La entrada es un archivo de texto. Cada linea contiene una lista de términos tipeados a mano por un usuario distinto. Generalmente los términos se encuentran separados por coma, pero no siempre. Además, cada término puede consistir en una o más palabras. Por ejemplo:
```
Cama, muebles tipo mesa de luz, escritorio, silla, billetera, libros.
cama  estante  frazadas  ventilador  lámpara  armario
Cama, sábanas, armario, silla, mesa de luz, velador, cortinas, TV,
```
 Los errores otográficos, de tipeo, o de estilo, deben ser considerados como variantes sinónimos y se debe resistir la tentación de editar el archivo de entrada para corregirlos. Esto causaría la duda de hasta qué punto el investigador alteró los datos a su propio gusto y el experimento sería menos confiable. El archivo de entrada sólo debe ser alterado por un algoritmo examinable. 


## Archivo 'datos': emprolijamiento automático

La estrategia es automatizar la mayor parte del proceso posible. Lo primero que realiza el programa es leer el archivo de entrada y crear una nueva versión emprolijada en un archivo llamado 'datos'. Por ejemplo, la primera linea se transforma probablemente en la segunda:

```
Mesa de Luz, cama. luz de Escritorio;   silla    para la ropa sucia
mesas_luz camas luz_escritorio silla_ropa_sucia
```

Los pasos involucrados son:
- Todas las letras se pasan a minúsculas.
- Se reemplazan o eliminan los caracteres especiales.
- Todos los separadores (puntos, comas, espacios de sobra) entre distintos términos son reemplazados por un espacio simple.
- Cada término se divide en subtérminos (palabras separadas por espacios). Un término puede consistir de un único subtérmino, o varios. Se eliminan todos los subtérminos innecesarios, y luego se unen los subtérminos restantes con guión bajo. Por ejemplo, 'mesa de luz' se separa en ['mesa', 'de', 'luz'], se elimina 'de', y el resultado final es 'mesa_luz'. La lista completa de subtérminos que siempre se elimina es la siguiente: 
```
que, de, del, el, la, los, las, un, uno, una, unos, unas
esa, ese, esas, esos, para, con, en, y, o, etc
algun, alguna, algunos, algunas, muchos, muchas
mas, otro, otra, otros, otras, cada, tipo, tipos
coso, cosito, varios, varias, suelto, suelta, sueltos, sueltas, set
largo, largos, larga, largas, corto, cortos, corta, cortas
viejo, viejos, vieja, viejas, comun, comunes, medida, medidas
color, colores, distinta, distintas, distinto, distintos, diferentes
tamaño, tamaños, grande, grandes, pequeño, pequeños, pequeña, pequeñas
gigante, gigantes, grueso, gruesa, fino, fina, finos, finas
chico, chicos, chica, chicas, chiquito, chiquita, chiquitos, chiquitas
```

- Hay otros subtérminos que también se eliminan, pero sólo en el caso de que el término esté compuesto de más de un subtérmino. Por ejemplo, el subtérmino 'caja'. Si el término completo es 'caja', no se elimina. Pero si el término completo es 'caja de fósforos', queda reducido a 'fósforos'. Los subtérminos que se eliminan en este caso son los siguientes:

```
'caja','cajas','cajitas','cajita','set','kit','tarro',
'articulos','productos','elementos','objetos'
``` 

- Si un término aparece repetido con versiones plurales y singulares en el mismo archivo, todas sus apariciones son reemplazadas por la versión plural. 
- Si un término es demasiado corto (tiene tres letras o menos) se considera error de tipeo y es eliminado.
- Si un término es demasiado largo (consiste en cinco o más subtérminos) se asume que el usuario no ingresó espacios adecuadamente y se separa en términos distintos. Por ejemplo, 'cama_mueble_escritorio_ventana_puerta' se separa en ['cama', 'mueble', 'escritorio', 'ventana', 'puerta']. Esto causa un efecto secundario indeseado, ya que en algunos casos los términos son del estilo 'abrigo antiguo de piel de la abuela guardado', lo cual termina separado en ['abrigo', 'antiguo', 'piel', 'abuela', 'guardado']. De todos modos, estos casos son excepciones y además no alteran la estadística final, ya que los términos resultantes suelen aparecer sólo una vez.


## Archivo 'vocabulario': identificación manual de sinónimos

Para indicar los términos que se cuentan como sinónimos se utiliza un archivo de texto separado, 'vocabulario'. Cada línea del archivo contiene una lista de términos que son sinónimos entre sí. Cada término es finalmente reemplazado por el que aparece primero en su misma línea. El archivo contiene el mismo conjunto de términos de la entrada, pero sin repeticiones. La primera vez que se ejecuta, cada término se ubica en una linea distinta. Es tarea del usuario ubicar a mano en la misma línea los términos que considera sinónimos.

Cada vez que se ejecuta, el programa modifica el archivo vocabulario para sincronizarlo con el archivo de entrada. El objetivo es que todos (y solamente) los términos de entrada se encuentren el archivo vocabulario. Si se borra a mano un término de 'vocabulario' pero sigue estando en la entrada, es agregado de nuevo. Y si se agrega un término en 'vocabulario' que no se encuentra en la entrada, es eliminado. En cualquier caso, los términos que se encuentran en la misma línea (indicando que son sinónimos) nunca son separados de modo automático, excepto que el usuario borre a mano un término, que luego es agregado por el programa a una linea nueva distinta.


## Archivo 'sugerencias': reconocimiento de sinónimos probables

El programa realiza sugerencias usando un criterio rudimentario pero útil. Si en el archivo de vocabulario hay dos términos que aún no se consideran sinónimos y uno de los dos contiene estrictamente al otro, se agregan a las sugerencias. Por ejemplo:

```
mesita
    mesita_luz

facturas
    facturas_boletas
    papelesfacturaspagas
    recibos_facturas

compu
    computadora
    computacion
    compu_escritorio
```

## Archivo 'cuenta': resultado final

La cuenta final es un archivo de texto. Cada línea corresponde a un término y sus respectivos sinónimos. Por ejemplo:

```
   491  94%  camas
   395  76%  mesitas_luz
   305  58%  almohadas
   297  57%  ropa
   271  52%  libros
   266  51%  frazadas
   265  51%  sabanas
   160  30%  placards
   155  29%  lamparas
   146  28%  sillas
   123  23%  zapatillas
   115  22%  tele
```

- Columna 1: Cantidad total de apariciones del término. Si varios términos que son sinónimos entre sí fueron ingresados por un mismo usuario (es decir, se encuentran en una misma linea del archivo de entrada y también en una misma línea del archivo vocabulario) se cuentan como una única aparición. 
- Columna 2: Porcentaje de los usuarios que ingresaron al menos una vez el término o uno de sus sinónimos. Es decir, de todos los usuarios, qué porcentaje de usuarios pensó en ese término o uno de sus sinónimos.  
- Columna 3: El término. De todo el conjunto de sinónimos equivalentes, el que se elije como representante es el que aparece primero al comienzo de la línea del archivo 'vocabulario'.


## Archivo 'grafico': nube de palabras

Existe la opción adicional de generar un gráfico de nube de palabras. Para esto el programa utiliza los módulos matplotlib y wordcloud, que se deben encontrar instalados. En caso de no ser necesario y no desear instalar estos módulos adicionales, se puede simplemente omitir esta opción.

La ubicación de cada palabra en el gráfico es aleatoria, de modo que ejecutando el programa sucesivas veces se obtienen resultados que varían en nivel estético.

Para generar el gráfico, al ejecutar el programa se agrega como segundo parámetro la letra 'g' luego del nombre de archivo.

```
$ python contador.py dormitorio.txt g
```



## Ciclo de trabajo eficiente recomendado

- Ejecutar el programa.
- Abrir o recargar los archivos 'cuenta', 'vocabulario' y 'sugerencias'.
- Recorrer el archivo 'cuenta', identificando sinónimos a mano. Como los términos se encuentran en orden de repeticiones, identificar los sinónimos que se encuentran más arriba altera más el resultado que identificar los que se encuentran más abajo. Además, los términos menos repetidos suelen ser errores de tipeo o palabras sin sentido.
- Editar el archivo 'vocabulario' con los sinónimos encontrados.
- Volver a ejecutar el programa.
- Recargar el archivo 'sugerencias' y recorrerlo. El orden es irrelevante.
- Editar nuevamente el archivo 'vocabulario' con los sinónimos encontrados.
- Volver a ejecutar el programa y repetir los pasos anteriores hasta quedar satisfecho. 
