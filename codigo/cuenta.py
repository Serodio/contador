from collections import Counter


def escribir(contador, datos, filename):
    contador_ordenado = contador.most_common()
    total = len(datos)
    with open(filename + '_cuenta', 'w') as f:
        for termino,cuenta in contador_ordenado:
            suma = str(cuenta).rjust(6, ' ') 
            porcentaje = str(cuenta * 100 / total).rjust(3, ' ') + '%'
            linea = suma + ' ' + porcentaje + '  ' + termino + '\n'
            f.write(linea)


def reducir_linea(lista, vocabulario):
    reemplazados = []
    for termino in lista:
        for grupo in vocabulario:
            if termino in grupo:
                reemplazados.append(grupo[0])
    return set(reemplazados)


def eliminar_repetidos(datos, vocabulario):
    return [t for linea in datos for t in reducir_linea(linea, vocabulario)]


def contar(datos, vocabulario):
    print 'contando repeticiones...'
    d = eliminar_repetidos(datos, vocabulario)
    return Counter(d)
