def leer(filename):
    try:
        with open(filename + '_vocabulario', 'r') as a:
            return [linea.strip().split(' ') for linea in a]
    except IOError:
        return []


def escribir(vocabulario, filename):
    with open(filename + '_vocabulario', 'w') as a:
        for sinonimos in vocabulario:
            linea = ' '.join(sinonimos) + '\n'
            a.write(linea)


def sincronizar(datos, vocabulario):
    print 'sincronizando datos y vocabulario...'
    voc_actualizado = []
    d = set([t for linea in datos for t in linea])
    v = set([t for linea in vocabulario for t in linea])
    voc_actualizado = [[t for t in sinonimos if t in d] for sinonimos in vocabulario]
    for t in d-v:
        voc_actualizado.append([t])
    return [v for v in voc_actualizado if v]
