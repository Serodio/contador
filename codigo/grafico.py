import random
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import cuenta


def grey_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    escala = 100 - (font_size / 8)
    l = escala/3
    return 'hsl(0, 0%%, %d%%)' % l


def graficar(contador, filename):
    wc = WordCloud(background_color='white', width=1600, height=800)
    wc = wc.generate_from_frequencies(contador)
    wc = wc.recolor(color_func=grey_color_func, random_state=3)
    plt.imshow(wc, interpolation='bilinear')
    plt.tight_layout(pad=0)
    plt.axis("off")
    plt.savefig(filename + '_grafico', dpi=200, format='png')
