#!/usr/bin/env python
# -*- coding: utf-8 -*-


TILDES = {
    'á':'a', 'é':'e', 'í':'i', 'ó':'o', 'ú':'u',
    'Á':'a', 'É':'e', 'Í':'i', 'Ó':'o', 'Ú':'u'
    }
CARACTERES = '¿?¡!()"'
SEPARADORES = ',./-;'
ELIMINAR = [
    '','que','de','del','el','la','los','las','un','uno','una','unos','unas',
    'algun','alguna','algunos','algunas','muchos','muchas',
    'varios','varias','suelto','suelta','sueltos','sueltas','set',
    'esa','ese','esas','esos','para','con','en','y','o','etc',
    'largo','largos','larga','largas','corto','cortos','corta','cortas',
    'viejo','viejos','vieja','viejas', 'comun','comunes','medida','medidas',
    'coso','cosito', 'mas','otro','otra','otros','otras','cada','tipo','tipos',
    'distinta','distintas','distinto','distintos','diferentes', 'color','colores',
    'gigante','gigantes','grueso','gruesa','fino','fina','finos','finas',
    'tamaño','tamaños','grande','grandes','pequeño','pequeños','pequeña','pequeñas',
    'chico','chicos','chica','chicas','chiquito','chiquita','chiquitos','chiquitas'
    ]
REDUNDANCIAS = [
    'caja','cajas','cajitas','cajita','set','kit','tarro',
    'articulos','productos','elementos','objetos'
    ]
NUMEROS = [str(i) for i in range(20)]
ELIMINAR += NUMEROS



def plural(t1, t2):
    return (t1 == t2 + 's'  or t1 == t2 + 'es' or 
            (t2.endswith('z') and t1 == t2[:-1] + 'ces'))


def plurales(t1, t2):
    return  t1 == t2 or plural(t1,t2) or plural(t2,t1)


def variante_plural(t1,t2):
    s1 = t1.split('_')
    s2 = t2.split('_')
    if len(s1) == len(s2):
        return all(plurales(s1[i],s2[i]) for i,x in enumerate(s1))
    return False


def variante_espaciada(t1,t2):
    return t1.replace('_', '') == t2.replace('_', '')


def es_variante(t1,t2):
    return variante_espaciada(t1,t2) or variante_plural(t1,t2)


def reemplazar_variante(termino, variantes):
    for grupo in variantes:
        if termino in grupo:
            return max(grupo, key=len)


def detectar_variantes(set_datos):
    variantes = []
    for t in set_datos:
        ya_incluida = False
        for g,grupo in enumerate(variantes):
            if any(es_variante(t,v) for v in grupo):
                variantes[g].append(t)
                ya_incluida = True
        if not ya_incluida:
            variantes.append([t])
    return variantes


def reemplazar_variantes(datos):
    d = set([t for linea in datos for t in linea])
    variantes = detectar_variantes(d)
    return [[reemplazar_variante(t, variantes) for t in linea] for linea in datos]



def eliminar_cortos(terminos):
    return [t for t in terminos if len(t)>3]


def separar_largos(terminos):
    nueva_lista = []
    for t in terminos:
        subterminos = t.split('_')
        if len(subterminos) > 4:
            nueva_lista += subterminos
        else:
            nueva_lista.append(t)
    return nueva_lista


def minimizar(termino):
    termino = termino.strip().lower()
    for t in TILDES:
        termino = termino.replace(t, TILDES[t])
    for c in CARACTERES:
        termino = termino.replace(c, '')
    subterminos = [s.strip() for s in termino.split() if s not in ELIMINAR]
    if len(subterminos) > 1:
        subterminos = [s for s in subterminos if s not in REDUNDANCIAS]
    return '_'.join(subterminos)


def parsear(linea):
    linea = linea.strip().strip(SEPARADORES)
    for s in SEPARADORES:
        linea = linea.replace(s,',')
    if linea.count(',') > 3:
        terminos = linea.split(',')
    else:
        linea = linea.replace(',',' ')
        terminos = linea.split(' ')
    terminos = [minimizar(t) for t in terminos]
    terminos = separar_largos(terminos)
    terminos = eliminar_cortos(terminos)
    return [t for t in terminos if t]


def leer(filename):
    print 'leyendo datos...'
    with open(filename, 'r') as f:
        datos = [parsear(linea) for linea in f]
        return reemplazar_variantes(datos)


def escribir(filename, datos):
    with open(filename + '_datos', 'w') as f:
        for fila in datos:
            f.write(' '.join(fila) + '\n')
