def escribir(sugerencias, filename):
    with open(filename + '_sugerencias', 'w') as f:
        for termino in sugerencias:
            f.write(termino + '\n')
            for sinonimo in sugerencias[termino]:
                f.write('\t' + sinonimo + '\n')
            f.write('\n')


def sugerir(vocabulario):
    print 'sugiriendo sinonimos...'
    sugerencias = {}
    for i,linea_i in enumerate(vocabulario):
        for k,linea_k in enumerate(vocabulario):
            if i!=k:
                for t1 in linea_i:
                    sug_t1 = [t2 for t2 in linea_k if t1 in t2]
                    if sug_t1:
                        sugerencias[t1] = sug_t1
    return sugerencias
