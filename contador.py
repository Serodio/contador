import sys
from codigo import datos, vocabulario, sugerencias, cuenta


if __name__ == '__main__':

    filename = sys.argv[1]

    d = datos.leer(filename)
    v = vocabulario.leer(filename)

    v = vocabulario.sincronizar(d,v)
    s = sugerencias.sugerir(v)
    c = cuenta.contar(d,v)

    datos.escribir(filename,d)
    vocabulario.escribir(v,filename)
    sugerencias.escribir(s,filename)
    cuenta.escribir(c,d,filename)

    if len(sys.argv)>2: 
        if sys.argv[2] == 'g':
            from codigo import grafico
            grafico.graficar(c,filename)
